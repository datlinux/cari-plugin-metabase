#!/usr/bin/env bash

set -euo pipefail

GH_REPO="https://github.com/metabase/metabase"
TOOL_NAME="metabase"
TOOL_TEST="metabase"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | awk '{print $2}'
}

list_github_tags() {
  curl -o $HOME/.metabase.html https://github.com/metabase/metabase/releases
  grep "<a href=" $HOME/.metabase.html |sed "s/<a href/\\n<a href/g" |sed 's/\"/\"><\/a>\n/2' | grep href | grep "\.jar" | grep -v "enterprise" | sort | uniq | cut -d'"' -f2 | cut -d"/" -f4 | cut -d"v" -f2 | grep -v -e "-RC"
  rm -f $HOME/.metabase.html
  #git ls-remote --tags --refs "$GH_REPO" |
  #  grep -o 'refs/tags/.*' | cut -d/ -f3- |
  #  sed 's/^v//'
}

list_all_versions() {  
	list_github_tags
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"
  rm -rf "$filename" &
  url="https://downloads.metabase.com/v${version}/metabase.jar"
  #url="$GH_REPO/releases/download/${version}/VSCodium-linux-x64-${version}.tar.gz"
  echo "* Downloading $TOOL_NAME release $version..."
  curl "${curl_opts[@]}" -o "$filename" -C - "$url" || fail "Could not download $url"
  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only"
  fi

  (
    mv $CARI_DOWNLOAD_PATH/metabase.jar $install_path/
    mkdir -p "$install_path/bin"
    cp -r "$CARI_DOWNLOAD_PATH"/* "$install_path"
    touch "$install_path/bin/metabase"
    echo "#!/bin/bash" >> "$install_path/bin/metabase"
    echo "export MB_JETTY_PORT=\"3105\"" >> "$install_path/bin/metabase"
    echo "java -jar $install_path/metabase.jar" >> "$install_path/bin/metabase"
    chmod a+x "$install_path/bin/metabase"
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
